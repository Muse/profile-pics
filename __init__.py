from st3m.application import Application, ApplicationContext
import st3m.run
import leds
import time
import random
import uos
from st3m.input import InputController, InputState

class ProfilePic(Application):
    input = InputController()
    count = 0
    pfps = []
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

    def draw(self, ctx: Context) -> None:
        ctx.image_smoothing = False
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        self.pfps = uos.listdir("/sd/profilepics")
        #count = random.randrange(0,len(pfps)-1)
        pfp = "/sd/profilepics/" + self.pfps[self.count]
        ctx.image(pfp, -120, -120, 241, 241)

        for i in range(40):
            if i % 8 == 0:
                # paint all tips 
                leds.set_rgb(i, 0, 0, 0)   
            elif i // 8 == 0:
                # yellow
                leds.set_rgb(i, 255, 255, 0)
            elif i // 8 == 1:
                # pink
                leds.set_rgb(i, 255, 0, 200)
            elif i // 8 == 2:
                # violet
                leds.set_rgb(i, 153, 50, 204)
            elif i // 8 == 3:
                # cyan
                leds.set_rgb(i, 0, 255, 255)
            elif i // 8 == 4:
                # green
                leds.set_rgb(i, 0, 255, 0)   
        

        leds.update()



    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)

        if self.input.buttons.app.left.pressed:
            if self.count == 0:
                self.count = len(self.pfps)-1
            else:
                self.count = self.count - 1
        elif self.input.buttons.app.right.pressed:
            if self.count == len(self.pfps)-1:
                self.count = 0
            else:
                self.count = self.count + 1

        super().think(ins, delta_ms) # Let Application do its thing

if __name__ == '__main__':
    st3m.run.run_view(ProfilePic(ApplicationContext()))

